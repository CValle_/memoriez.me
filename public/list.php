<?php
$maxauthlen = 10;

    // get_filenames, obtiene los nombres de los artículos en la carpeta articles
    function get_filenames($directory) {
        $filenames = array();
        $directory_obj = opendir($directory);
        while(false != ($filename = readdir($directory_obj))) {
            if(($filename != ".") && ($filename != "..") && ($filename != ".gitkeep")) {
                $filenames[] = $filename; // put in array
            }
        }
        if(count($filenames) > 0){
            natsort($filenames); // ordenamos alfabeticamente
            $filenames = array_reverse($filenames); // le damos la vuelta a la ordenación anterior
        }
        return $filenames;
    }

    function format_date($datetime){
        $re = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/m';

        preg_match_all($re, $datetime, $matches, PREG_SET_ORDER, 0);

        // Print the entire match result
        //var_dump($matches);

        $formateddate = $matches[0][1] . "-" . $matches[0][2] . "-" . $matches[0][3];
        // . " " . $matches[0][4] . ":" . $matches[0][5] . ":" . $matches[0][6];
        return $formateddate;
    }

    function concat_spaces($auth){
        global $maxauthlen;

        $dif = $maxauthlen - strlen($auth);
        if ($dif > 0){
            for ($i = 0; $i < $dif; $i++) {
                $auth .= "&nbsp;";
            }
        }
        return $auth;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="favicon.png">

        <title>list · memoriez</title>
        
        <meta name="author" content="CValle_"> <!-- This site was made by https://gitlab.com/CValle_ -->
        <meta name="description" content="You can view your memories here!">
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="list · memoriez" />
        <meta property="og:description" content="You can view your memories here!" />
        <meta property="og:url" content="https://memoriez.me/list.php" />
        <meta property="og:site_name" content="memoriez.me" />
        <!-- <meta property="og:image" content="" /> -->
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" href="default.css">
        <link rel="stylesheet" href="list.css">

        <script>
            function validate() {
                let titleIn = document.getElementById("title");
                let textIn = document.getElementById("text");

                if(titleIn.value == "" || textIn.value == ""){
                    titleIn.className += " empty";
                    textIn.className += " empty";
                    return false;
                }
                return true;
            }
        </script>

    </head>
    <body>
        <header>
            <div id="contheader">
                <a id="imglink" href="index.php"><img src="img/logoMemo.png" alt="LOGO"></a>
                <a id="link" href="index.php">EDIT</a>
            </div>
        </header>
        <main>
            <table id="tablelist">
                <?php
                    $filenames = get_filenames("files/");
                    $private = false;

                    foreach ($filenames as &$file) {
                        $filename = substr($file, 0, -4);
                        $filename = str_replace("_", " ", $filename);
                        $data = explode("-", $filename);
                        $datetime = $data[0];
                        $auth = strtoupper($data[1]);
                        $title = ucfirst($data[2]);

                        $auth = concat_spaces($auth);
                        $datetime = format_date($datetime);
                        $url = "view.php?file=" . $file;

                        printf('<tr>');

                        if($private == true) printf('<td class="private">×</td>');
                        else printf('<td class="private">&nbsp;</td>');

                        printf('<td><a href="%s" class="listitem">[%s] <span class="title">%s</span> · %s</a></td>', $url, $auth, $title, $datetime);
                        printf('</tr>');
                    }
                ?>
            </table>
        </main>
        <footer>
            <p>
                <a href="https://gitlab.com/CValle_/memoriez.me">GIT</a> ·
                <a href="https://www.gnu.org/licenses/gpl-3.0.txt"> GPL3</a>
            </p>
        </footer>
    </body>
</html>