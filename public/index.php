<!--                                 _          
 _ __ ___   ___ _ __ ___   ___  _ __(_) ___ ____
| '_ ` _ \ / _ \ '_ ` _ \ / _ \| '__| |/ _ \_  /
| | | | | |  __/ | | | | | (_) | |  | |  __// / 
|_| |_| |_|\___|_| |_| |_|\___/|_|  |_|\___/___|

- By https://gitlab.com/CValle_
-->


<?php
    date_default_timezone_set('UTC');

    // No se ponen todos por que se asumen como definidos con el titulo
    if (isset($_POST["title"])) {
        $title = str_replace("/","",trim($_POST["title"]));
        $auth = str_replace("/","",trim($_POST["auth"]));
        $text = $_POST["text"];

        if ($title != "" && $text != "") {
            $datetime = date('YmdHis');
            $filename = "files/" . $datetime . "-" . $auth . "-" . $title . ".txt";
            $repfilename = str_replace(" ", "_", $filename);
            file_put_contents($repfilename, $text);
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="favicon.png">

        <title>edit · memoriez</title>
        
        <meta name="author" content="CValle_"> <!-- This site was made by https://gitlab.com/CValle_ -->
        <meta name="description" content="You can write your memories here!">
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="edit · memoriez" />
        <meta property="og:description" content="You can write your memories here!" />
        <meta property="og:url" content="https://memoriez.me" />
        <meta property="og:site_name" content="memoriez.me" />
        <!-- <meta property="og:image" content="" /> -->
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" href="default.css">
        <link rel="stylesheet" href="index.css">

        <script>
            function validate() {
                let titleIn = document.getElementById("title");
                let textIn = document.getElementById("text");

                if(titleIn.value == "" || textIn.value == ""){
                    titleIn.className += " empty";
                    textIn.className += " empty";
                    return false;
                }
                return true;
            }
        </script>

    </head>
    <body>
        <header>
            <div id="contheader">
                <a id="imglink" href="index.php"><img src="img/logoMemo.png" alt="LOGO"></a>
                <a id="link" href="list.php">LIST</a>
            </div>
        </header>
        <main>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return validate();">
                <h1>MEMO</h1>
                <p>
                    <div id="cont">
                        <input id="title" name="title" maxlength="100" class="editbox oneline no_default" type="text" placeholder="TITLE">
                        <input id="auth" name="auth" maxlength="10" class="editbox oneline no_default" type="text" placeholder="AUTHOR">
                        <textarea id="text" name="text" maxlength="5000" class="editbox no_default" placeholder="WRITE YOUR MEMORY"></textarea>
                    </div>
                </p>
                <p id="contbutton">
                    <input type="submit" value="POST" class="no_default button_style">
                </p>
            </form>
        </main>
        <footer>
            <p>Please don't write anything that could harm yourself or anyone else.</p>
            <p>
                <a href="https://gitlab.com/CValle_/memoriez.me">GIT</a> ·
                <a href="https://www.gnu.org/licenses/gpl-3.0.txt"> GPL3</a>
            </p>
        </footer>
    </body>
</html>