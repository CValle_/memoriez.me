<?php
    $filename = "";
    if(isset($_GET["file"])){
        $filename = $_GET["file"];
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="favicon.png">

        <title>view · memoriez</title>
        
        <meta name="author" content="CValle_"> <!-- This site was made by https://gitlab.com/CValle_ -->
        <meta name="description" content="Your specific memory">
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="view · memoriez" />
        <meta property="og:description" content="Your specific memory" />
        <meta property="og:url" content="https://memoriez.me/view.php" />
        <meta property="og:site_name" content="memoriez.me" />
        <!-- <meta property="og:image" content="" /> -->
        <meta name="robots" content="index, follow" />

        <link rel="stylesheet" href="default.css">
        <link rel="stylesheet" href="view.css">

    </head>
    <body>
        <header>
            <div id="contheader">
                <a id="imglink" href="index.php"><img src="img/logoMemo.png" alt="LOGO"></a>
                <a id="link" href="list.php">LIST</a>
            </div>
        </header>
        <main>
            <h1>
                <?PHP
                    $file = substr($filename, 0, -4);
                    $file = str_replace("_", " ", $file);
                    $data = explode("-", $file);
                    echo ucfirst($data[2]);
                ?>
            </h1>
            <pre class="wrapped">
<?php echo file_get_contents("files/".$filename);?>
            </pre>
        </main>
        <footer>
            <p>
                <a href="https://gitlab.com/CValle_/memoriez.me">GIT</a> ·
                <a href="https://www.gnu.org/licenses/gpl-3.0.txt"> GPL3</a>
            </p>
        </footer>
    </body>
</html>